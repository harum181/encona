<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sample admin website
        App\User::create([
        	'name' => 'Brian Adavianta',
        	'email' => 'brian@adav.com',
        	'password' => bcrypt('secret')
        ]);
    }
}
