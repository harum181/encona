<?php

use Illuminate\Database\Seeder;

class ManagementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sample management photo
    		App\Management::create([
    			'photo' => '1.jpg',
    			'title' => 'Ir. Ary Mochtar Pedju. M.Arch',
                'position' => 'Chairman of The Board',
                'about_id' => '1'
    		]);
    		App\Management::create([
    			'photo' => '1.jpg',
    			'title' => 'Ir. Boediono Soerasno',
                'position' => 'Commissioner',
                'about_id' => '1'
    		]);
    		App\Management::create([
    			'photo' => '1.jpg',
    			'title' => 'Ir. George The',
                'position' => 'Pres. Commissioner',
                'about_id' => '1'
    		]);
    		App\Management::create([
    			'photo' => '1.jpg',
    			'title' => 'Ir. Edno Djoko Windratno',
                'position' => 'Commissioner',
                'about_id' => '1'
    		]);
    		App\Management::create([
    			'photo' => '1.jpg',
    			'title' => 'Ir. JB. Visnu Sulistyawan',
                'position' => 'Director',
                'about_id' => '1'
    		]);
    		App\Management::create([
    			'photo' => '1.jpg',
    			'title' => 'Ir. Haryono Y.B.',
                'position' => 'President Director',
                'about_id' => '1'
    		]);
    		App\Management::create([
    			'photo' => '1.jpg',
    			'title' => 'Ir. Sayoeti Sukamdi',
                'position' => 'Director',
                'about_id' => '1'
    		]);

            $from = database_path() . DIRECTORY_SEPARATOR . 'seeds' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
            $to = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
            File::copy($from . '1.jpg', $to . '1.jpg');
    }
}
