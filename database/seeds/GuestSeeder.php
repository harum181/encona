<?php

use Illuminate\Database\Seeder;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Guest::create([
    			'name' => 'Sample user',
    			'email' => 'sample@gmail.com',
          'comment' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum sit, excepturi est modi, eum dignissimos eos, explicabo nobis architecto laboriosam dicta! Ipsum dolorum eos autem aspernatur illo nam, suscipit corrupti?'
    		]);
    }
}
