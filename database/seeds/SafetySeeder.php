<?php

use Illuminate\Database\Seeder;

class SafetySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Safety::create([
    			'type' => '1',
    			'image' => 'hse.png',
          'name' => 'HSE Safety'
    		]);
        App\Safety::create([
          'type' => '1',
          'image' => 'hse.png',
          'name' => 'HSE Safety Award'
        ]);
    		App\Safety::create([
    			'type' => '2',
    			'image' => 'hse.png',
          'name' => 'Achievement'
    		]);
    		App\Safety::create([
    			'type' => '3',
    			'image' => 'hse.png',
          'name' => 'Safety'
    		]);

    		$from = database_path() . DIRECTORY_SEPARATOR . 'seeds' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
        $to = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
        File::copy($from . 'hse.png', $to . 'hse.png');
    }
}
