<?php

use Illuminate\Database\Seeder;

class BusinessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sample business page
    		App\Business::create([
    			'image' => 'heart.png',
    			'title' => 'EPC Services',
    			'content' => 'Design and Engineering documents are products of Design Architect /Engineers for use as the basis for field construction executed by Contractors. Design and Engineering is a complex and detailed process to turn ideas into reality. It involves technical, practical and economical considerations presented in the form of technical drawings, specification documents, bill of quantities and contractual conditions.'
    		]);
    		App\Business::create([
    			'image' => 'heart.png',
    			'title' => 'Fabrication Services',
    			'content' => 'In the late of 2011, Encona took over a fabrication workshop from PT. Permiko at Cilacap, Central Java. The objectives of this aquisition are as follows : For the medium term is to synergize the capabilities of the corporate which had earned some construction projects in Cilacap. For the long term is to expand the business line or diversification in fabrication and equipment services.'
    		]);
    		App\Business::create([
    			'image' => 'heart.png',
    			'title' => 'Trading Services',
    			'content' => 'Ells Trading Division already serves domestic needs of asphalt by utilizing and managing integrated operational activities. The division was established in early 2009, with activities of supplying asphalt in bulk, drums, bag or using bitutainer to the customers, direct from the storage facilities in Medan, Dumai, Pekanbaru, Jakarta, Cirebon, Semarang, Surabaya and Gresik. We offer various asphalt product, from Pertamina or imported brands, either in bulk, drums, polybag, or bitutainer. We also develop modified asphalt products with proven track record after being used for road projects in Indonesia.'
    		]);
    		App\Business::create([
    			'image' => 'heart.png',
    			'title' => 'Petroleum Services',
    			'content' => 'Our service is well testing, production processing, subsurface testing, field exploration and development activities. Our services provide the whole package including: separator, tanks, gas compressors, transfer pump, water treatment and portable camps. Our field personnel are highly trained and experienced in both onshore and offshore operations. We fabricate and maintain all of our equipment at our workshop to the highest standards of health, safety and quality so that our clients can obtain the accurate results required to determine the productivity and processing of oil and gas fields. We offer our clients a professional outsourcing service providing experienced earth scientists, welltest geologists, drilling engineers, reservoir engineers, production engineers and technicians for the upstream industry. Another valuable service which we provide is heavy equipment rental.We can supply offshore crane rental for any offshore operations including platform installation and complete crane overhaul.The crane overhaul involves removing the crane from the platform in order to perform the necessary refurbishments and in addition we provide another crane so our client can continue operations with minimum downtime.Maintenance, spare parts, emergency call out and providing the crane operators required is also part of the service.'
    		]);
    }
}
