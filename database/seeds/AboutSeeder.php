<?php

use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sample about us
        App\About::create([
        	't_about' => 'About Encona',
        	'c_about' => 'PT Encona Inti Industri (EII), a multi-disciplinary EPC firm, was established in 1981, as one of Encona-group’s member. The company, with Engineering, Procurement, and Construction services as its core, is ideally suited to play a major role in Indonesian and global modern construction industries.Since its founding at the beginning of Indonesia’s massive development activities, EII has been unique with its knowledge-based approach services. EII strives to become a world class top EPC contractor.The company’s broad array of services covers all project phases, and range from basic determination of client’s needs, to post operational and maintenance programs. Our technological solutions for the complex construction industry problems always include endeavors to protect the environment.EII has the capabilities to design, build, and maintain new townships, highways, bridges, airports, power plants and transmission networks, industrial facilities, housing, office buildings and environmental control systems. In the last few year, aside from developing its business in the Oil and Gas industry, recently EII also enter the Petroleum Services.',
        	't_milestone' => 'Milestone',
        	'p_milestone' => 'milestone.png',
        	't_management' => 'Management'
        ]);

        $from = database_path() . DIRECTORY_SEPARATOR . 'seeds' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
        $to = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
        File::copy($from . 'milestone.png', $to . 'milestone.png');
    }
}
