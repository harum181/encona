<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(BusinessSeeder::class);
        $this->call(ManagementsSeeder::class);
        $this->call(ExperienceSeeder::class);
        $this->call(GuestSeeder::class);
        $this->call(SafetySeeder::class);
    }
}
