$(document).ready(function () {

// Sweetalert for submit
$(document.body).on('click', '.submit-confirm', function (event) {
	event.preventDefault()
		var $form = $(this).closest('form')
		var $el = $(this)
		var text = $el.data('confirm-message') ? $el.data('confirm-message') : 'Proses submit data telah berhasil'
		
		swal({
				title: 'Sukses',
				text: text,
				type: 'success',
				confirmButtonColor: '#50d735',
				confirmButtonText: 'OK',
				closeOnConfirm: true
		},
		function () {
			$form.submit()
		})
})

// Select style
$('.js-selectize').selectize({
	sortField: 'text'
})


});