{{-- <h1>jelo</h1> --}}
@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    Callout
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Update Management Page</h3>
    </div>
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-body">

        {!! Form::model($business, ['url' => ['business', $business], 'method' => 'post'])!!}  
          {!! Form::token(); !!}

            <div class="form-group">
              {!! Form::label('title', 'Title section one') !!}
              {!! Form::text('title', null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('content', 'Content business') !!}
              {!! Form::textarea('content', null, ['class'=>'form-control']) !!}
            </div>
            <a href="{{ url('business') }}" class="btn btn-warning">Batal</a>
            {!! Form::submit(isset($model) ? 'Update' : 'Simpan', ['class'=>'btn btn-primary'] ) !!}

        {!! Form::close() !!}

        </div>
        </div>
      </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/about_us')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection