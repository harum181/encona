@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Our Business Page</h3>
    </div>
    <div class="col-md-8">
      
      <div class="panel panel-default tab-top">
        <div class="panel-body">
          
          <table style="margin: 0;" class="table table-bordered table-stripped">
            <thead>
              <tr>
                <td><strong>Title</strong></td>
                <td><strong>Content</strong></td>
                <td><strong>Action</strong></td>
              </tr>
            </thead>
            <tbody>
              @foreach($businesses as $business)
              <tr>
                <td>{{ $business->title }}</td>
                <td>{{ $business->content }}</td>
                <td>
                  <a href="{{ url('business/edit', $business->id)}}" style="font-size: 10px;" class="btn btn-primary">Update info</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/our_business')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection
