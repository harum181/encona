@extends('layouts.app')
@section('content')

<!--====================== EXPERIENCES (Page Content) ===================-->
  <div class="container-fluid">
         <h1 align="left" style="margin-left: 70px; color:#ab0902; margin-top: 100px;">Experiences</h1>
  </div><br><br>


 <!--=========================== Table Content =======================-->
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-bordered table-hover table-striped" style="margin: 0;">
        <thead class="custom-table">
          <tr>
            <th>CLIENT NAME</th>
            <th>PROJECT TITLE</th>
            <th>SECTOR</th>
            <th>SERVICE</th>
            <th>LOCATION</th>
            <th>YEAR</th>
          </tr>
        </thead>
        <tbody>
          @foreach($experiences as $experience)
          <tr>
          <td>{{ $experience->client_name }}</td>
          <td>{{ $experience->project_title }}</td>
          <td>{{ $experience->sector }}</td>
          <td>{{ $experience->service }}</td>
          <td>{{ $experience->location }}</td>
          <td>{{ $experience->year }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="col-md-12">
      {{ $experiences->links() }}
    </div>
  </div>
</div>

@endsection