@extends('layouts.app')
@section('content')

<!--====================== Our Business (Page Content) ==================-->
  <div class="container-fluid">
         <h1 align="left" style="margin-left: 70px; color:#ab0902; margin-top: 100px;">Career</h1>
  </div><br><br>


 <!--=========================== Services Content =======================-->
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4">
      <img src="./img/Career1.jpg">
    </div>
    <div class="col-sm-8 career">
      <h1 style="margin-left: 150px;margin-right:150px;margin-top: 100px"><strong>ENGINEERS</strong></h1>
      <img style="margin-top: -78px;margin-left: 360px" src="./img/Design 2.png">
      <p style="margin-left:150px;margin-right:150px;font-size: 14pt;" align=justify>Engineer working in the field of construction and preparation for construction, such as piping, plate cutting, welding, and installation of all construction devices.</p>
      <img style="margin-left: 150px;margin-top: 80px" src="./img/Design 6.png">
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4">
      <img style="margin-left:500px" src="./img/Career4.png">
    </div>
    <div class="col-sm-8 career">
      <h1 style="margin-left:180px;margin-top: 200px"><strong>LOGISTICS</strong></h1>
      <img style="margin-top:-70px;margin-left: 30px" src="./img/Design 5.png">
      <p style="margin-left:-200px;margin-right:500px;font-size: 14pt;" align=justify>Logistics workers working in the field of corporate spending data collection, data collection of goods in and out, and fully responsible for the company's inventory.</p>
      <img style="margin-left: -270px;margin-top: 100px" src="./img/Design 73.png">
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4">
      <img src="./img/Career3.png">
    </div>
    <div class="col-sm-8 career">
      <h1 style="margin-left: 50px;margin-right:250px;margin-top: 200px;"><strong>FINANCE</strong></h1>
      <img style="margin-top: -78px;margin-left: 200px" src="./img/Design 2.png">
      <p style="margin-left:50px;margin-right:250px;font-size: 14pt;" align=justify>The finance department is responsible for the company's financial statements, financial transactions, as well as payments to workers.</p>
      <img style="margin-left: 400px" src="./img/Design 8.png">
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4">
      <img style="margin-left: 700px" src="./img/Career2.png">
    </div>
    <div class="col-sm-8 career">
      <h1 style="margin-left:295px;margin-top:200px"><strong>HRD</strong></h1>
      <img style="margin-top:-70px;margin-left: 140px" src="./img/Design 5.png">
      <p style="margin-left:-200px;margin-right:500px;font-size: 14pt;" align=justify>Human Resources Department is responsible for the development of quality Encona workers, on the other hand is also responsible to the students who do field work in the company.</p>
      <img style="margin-left: -450px" src="./img/Design 9.png">
    </div>
  </div>
</div>
<a style="margin-left: 1070px " href="http://www.freepik.com">Designed by Freepik</a>

@endsection