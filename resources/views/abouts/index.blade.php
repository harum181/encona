@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">About Page</h3>
    </div>
    <div class="col-md-8">
      <div class="panel-titleo">
        <a href="{{ url('abouts') }}" class="tabs {{ url('abouts') == request()->url() ? 'tabs-active' : '' }}">Content Page</a>
        <a href="{{ route('abouts.edit', $about->id)}}" class="tabs {{ url('about_update') == request()->url() ? 'tabs-active' : '' }}">Update page</a>
      </div>


      <div class="panel panel-default tab-tope">
        <div class="panel-body">

          <label for="">Title section</label>
          <div class="form-control" style="margin-bottom: 10px;">{{ $about->t_about }}</div>
          <label for="">{{ $about->t_about }} section content</label>
          <div class="form-control" style="height: 100% !important; margin-bottom: 10px;">{{ $about->c_about }}</div>
          <label for="">Title {{ $about->t_milestone }}</label>
          <div class="form-control" style="margin-bottom: 10px; margin-bottom: 10px;">{{ $about->t_milestone }}</div>
          <label for="">{{ $about->t_milestone }} section content</label>
          <div class="form-control" style="height: 100% !important; margin-bottom: 10px;"><img src="{{ url('/img/'.$about->p_milestone) }}" class="img-rounded index-img" style="width: 100%;"></div>
          <label for="">Title {{ $about->t_management }}</label>
          <div class="form-control" style="margin-bottom: 10px; margin-bottom: 10px;">{{ $about->t_management }}</div>

          {{-- @foreach($managements as $management)
          <div class="col-md-4" style="padding-left: 0">
            <div class="panel panel-default">
              <div class="panel-heading fix-title">{{ $management->title }}</div>
              <div class="panel-body" style="padding: 20px;"><img src="{{ url('/img/'.$management->photo) }}" class="img-rounded index-img" style="width: 100%;"></div>
              <div class="panel-footer fix-title">
                <a href=""></a>
                {{ $management->position }}
              </div>
            </div>
          </div>
          @endforeach --}}

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/about_us')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection
