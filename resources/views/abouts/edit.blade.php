{{-- <h1>jelo</h1> --}}
@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Update About Page</h3>
    </div>
    <div class="col-md-8">
      <div class="panel-titleo">
        <a href="{{ url('abouts') }}" class="tabs {{ url('abouts') == request()->url() ? 'tabs-active' : '' }}">Content Page</a>
        <a href="{{ route('abouts.edit', $about->id)}}" class="tabs {{ url('abouts/1/edit') == request()->url() ? 'tabs-active' : '' }}">Update page</a>
      </div>


      <div class="panel panel-default tab-tope">
        <div class="panel-body">

        {!! Form::model($about, ['route' => ['abouts.update', $about], 'method' => 'patch', 'files' => true])!!}  
          {!! Form::token(); !!}

            <div class="form-group">
              {!! Form::label('t_about', 'Title section one') !!}
              {!! Form::text('t_about', null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('c_about', 'Content about') !!}
              {!! Form::textarea('c_about', null, ['class'=>'form-control', 'style'=>'height: 200px;']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('t_milestone', 'Title section two') !!}
              {!! Form::text('t_milestone', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
            {!! Form::label('t_milestone', 'Content section two') !!}
            
            @if(isset($about) && $about->p_milestone !== '')
              <div class="row">
                <div class="col-md-12">
                  <div class="thumbnail">
                    <img src="{{ url('/img/'.$about->p_milestone) }}" class="img-rounded">
                  </div>
                </div>
              </div>
            @endif
            </div>

            <div class="form-group">
            {!! Form::label('p_milestone', 'Display milestone (jpg, jpeg, png)') !!}
            {!! Form::file('p_milestone') !!}
            </div>

            <div class="form-group">
              {!! Form::label('t_management', 'Content milestone') !!}
              {!! Form::text('t_management', null, ['class'=>'form-control']) !!}
            </div>
            
            <a href="{{ route('abouts.index') }}" class="btn btn-warning">Batal</a>
            {!! Form::submit(isset($model) ? 'Update' : 'Simpan', ['class'=>'btn btn-primary'] ) !!}

        {!! Form::close() !!}

        {{-- {!! Form::model($management, ['route' => ['abouts.update_management', $management], 'method' => 'patch', 'files' => true])!!}   --}}
        {{-- {!! Form::token(); !!}         --}}
      
          {{-- @foreach($managements as $management)
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body"></div>
                <div class="panel-footer"></div>
              </div>
            </div>
          @endforeach --}}

        {{-- {!! Form::close() !!} --}}

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/about_us')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection