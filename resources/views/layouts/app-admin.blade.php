<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PT Encona Inti Industri</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <link href="{{ url('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container-fluid">
      <div class="row">
        <div class="upper-top top">
          <div class="upper-left">
            Administrator dashboard
          </div>
          <div class="upper-right">
            <a href="{{ url('logout') }}" class="link-logout">
              <i class="fa fa-close inline-btn"></i>
              <span class="logout">Logout</span>
            </a>
          </div>
        </div>
      </div>
      <div class="row">

        <div class="def-panel-left">
          <div class="col-md-2 save-padding side">
            <div class="side-header">
              <i class="fa fa-gg-circle icon-encona"></i>
              <div class="title-dash">ENCONA PANEL</div>
            </div>
            <div class="side-content">
             <a href="{{ url('home') }}" class="{{ url('home') == request()->url() ? 'actives' : '' }}"><i class="fa fa-bars fa-margin"></i>Dashboard</a>
             <a href="{{ url('guests') }}" class="{{ url('guests') == request()->url() ? 'actives' : '' }}"><i class="fa fa-window-maximize fa-margin"></i>Guest Book</a>
             <a href="{{ url('abouts') }}" class="{{ url('abouts') == request()->url() ? 'actives' : '' }}"><i class="fa fa-calendar-o fa-margin"></i>About</a>
             <a href="{{ url('managements') }}" class="{{ url('managements') == request()->url() ? 'actives' : '' }}"><i class="fa fa-copyright fa-margin"></i>Management</a>
             <a href="{{ url('business') }}" class="{{ url('business') == request()->url() ? 'actives' : '' }}"><i class="fa fa-clone fa-margin"></i>Our Business</a>
             <a href="{{ route('experiences.index') }}" class="{{ url('experiences') == request()->url() ? 'actives' : '' }}" ><i class="fa fa-envelope-o fa-margin"></i>Experience</a>
             <a href="{{ route('safeties.index') }}" class="{{ url('safeties') == request()->url() ? 'actives' : '' }}"><i class="fa fa-life-ring fa-margin"></i>Safety</a>
             <a href="{{ url('#') }}"><i class="fa fa-tasks fa-margin"></i>Career</a>      
            </div>
          </div>
        </div>
        <div class="def-panel-right">
          <div class="col-md-10 save-padding right">
            @yield('admin')
          </div>
        </div>

      </div>
    </div>


    <!-- JavaScripts -->
    <script src="{{ elixir('js/all.js') }}"></script>
</body>
</html>
