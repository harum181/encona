<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PT Encona Inti Industri</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <link href="{{ url('css/style.css') }}" rel="stylesheet">

</head>
<body>
    <!--===================== Fungsi Smooth Scrolling =====================-->
    {{-- <script>
    $(document).ready(function(){
      // Add smooth scrolling to all links in navbar + footer link
      $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();

          // Store hash
          var hash = this.hash;

          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 900, function(){
     
            // Add hash (#) to URL when done scrolling (default click   behavior)
            window.location.hash = hash;
          });
        } // End if
      });
    
      $(window).scroll(function() {
        $(".slideanim").each(function(){
          var pos = $(this).offset().top;

          var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
              $(this).addClass("slideA");
            }
        });
      });

      $(window).scroll(function() {
        $(".slideanim2").each(function(){
          var pos = $(this).offset().top;

          var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
              $(this).addClass("slideB");
            }
        });
      });
    })
    </script> --}}

    <style type="text/css">
      .parallax {
      /* The image used */
      background-image: url("./img/BackgroundCustFIX.png");

      /* Set a specific height */
      min-height: 500px; 

      /* Create the parallax scrolling effect */
      background-attachment: fixed;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;}

       .parallax2 {
      /* The image used */
      background-image: url("./img/2990.jpg");

      /* Set a specific height */
      min-height: 100%; 

      /* Create the parallax scrolling effect */
      background-attachment: fixed;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;}
    </style>
    <!-- Navigation --> 
    <!-- Logo, nama website, dan tagline disini -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
          <a class="navbar-brand" href="{{ url('homepage') }}" title="Home">
            <img src="{{ url("img/logo2_3.png") }}" alt="Home">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right buttoncust-nav">
            {{-- <li><a href="{{ url('homepage') }}">Home</a></li> --}}
            <li><a class="{{ url('about_us') == request()->url() ? 'menu-active' : '' }}" href="{{ url('about_us') }}">About Us</a></li>
            <li><a class="{{ url('our_business') == request()->url() ? 'menu-active' : '' }}" href="{{ url('our_business') }}">Our Business</a></li>
            <li><a class="{{ url('experience') == request()->url() ? 'menu-active' : '' }}" href="{{ url('experience') }}">Experiences</a></li>
            <li><a href="{{ url('safety') }}">Safety</a></li>
            <li><a href="{{ url('career') }}">Career</a></li>
          </ul>
        </div>
      </div>
     </nav>
     <style>
      .dashe:hover{
        opacity: 1;
        color: white;
        position: fixed;
        bottom: 0px;
      }
      .dashe{
        transition: 500ms all ease;
        /*opacity: .3;*/
        padding: 10px 20px;
        position: fixed;
        bottom: -42px;
        right: 10px;
        z-index: 4000;
        background-color: #ff9800;
        border-top: 4px solid white;
        border-left: 4px solid white;
        border-right: 4px solid white;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        color: white;
        box-shadow: 0px 0px 20px 0px rgba(86, 86, 86, 0.45);
       }
     </style>
      @if(Auth::user())
        <a href="{{ url('home') }}" class="dashe">
          <i class="fa fa-eye fa-margino"></i>
          Live preview
          <span style="display: block; margin-top: 10px; border-top: 1px solid white; padding-top: 10px;">Back to dashboard</span>
        </a>
      @endif

    @yield('content')

    <!--============================= FOOTER ===============================--> 

    <!-- The content of your page would go here. -->

    <footer class="footer-distributed">

      <div class="footer-left">

        <h3 style="font-family: 'Open Sans">PT Encona<span> Inti Industri</span></h3>
        <p class="footer-company-name">All Rights Reserved PT Encona Inti Industri. &copy; 2017</p>
      </div>

      <div class="footer-center">

        <div>
          <i class="fa fa-map-marker"></i>
          <p><span>Jl. T.B. Simatupang Kav.18</span> Pasar Minggu, Jakarta Selatan 12520</p>
        </div>

        <div>
          <i class="fa fa-phone"></i>
          <p><span>Phone : (62-21) 2278 3300</span><span>Facs : (62-21) 2275 3005</span>
          </p>
        </div>

        <div>
          <i class="fa fa-envelope"></i>
          <p><a href="mailto:enconaii@indo.net.id">enconaii@indo.net.id</a></p>
        </div>

      </div>

      <div class="footer-right">

        <p class="footer-company-about">
          <span>About the company</span>
          PT Inti Encona Industries is a company engaged in the construction field. Provide three main services, which is Engineering, Procurement, and Construction. Good engineers, reliable purchasing, high-quality of construction is an advantage that owned by the company.
        </p><br>

         <p><a href="#myPage" title="To Top">Back to top</a></p>
      </div>

    </footer>
    
    <!-- JavaScripts -->
    <script src="{{ elixir('js/all.js') }}"></script>
</body>
</html>
