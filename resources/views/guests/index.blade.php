@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Guest Book</h3>
    </div>
    <div class="col-md-8">

      <div class="panel panel-default tab-top">
        <div class="panel-body">
          {!! Form::open(['url' => 'guest_search', 'method'=>'get', 'class'=>'form-inline']) !!}
          <div class="form-group">
            {!! Form::text('keyword', isset($keyword) ? $keyword : null, ['class'=>'form-control', 'placeholder' => 'Cari nama guest...']) !!}
          </div>
          {!! Form::button('<i class="fa fa-search"></i>', ['type' => 'submit', 'class'=>'btn btn-primary']) !!}
          {!! Form::close() !!}
          <hr>
          <table style="margin: 0;" class="table table-bordered table-stripped">
            <thead>
              <tr>
                <td><strong>Name</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>Comment</strong></td>
                <td><strong>Tanggal</strong></td>
                <td style="width: 0%;"><strong>Action</strong></td>
              </tr>
            </thead>
            <tbody>
              @forelse($guests as $guest)
              <tr>
                <td>{{ $guest->name }}</td>
                <td>{{ $guest->email }}</td>
                <td>{{ $guest->comment }}</td>
                <td>{{ date('d F Y', strtotime($guest->created_at)) }}</td>
                <td>
                  {!! Form::model($guest, ['url' => ['guest_destroy', $guest], 'method' => 'delete', 'class' => 'form-inline text-center'] ) !!}
                  {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger js-delete-confirm text-center']) !!}
                  {!! Form::close()!!}
                  {{-- <a href="{{ url('business/edit', $business->id)}}" style="font-size: 10px;" class="btn btn-primary">Update info</a> --}}
                </td>
              </tr>
              @empty
                <tr>
                  <td colspan="4" style="text-align: center;">Data <strong>{{ $keyword }}</strong> tidak ditemukan</td>
                </tr>
              @endforelse
            </tbody>
          </table>
            
          {{ $guests->links() }}

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('homepage')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection
