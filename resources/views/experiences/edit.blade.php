@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    Callout
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Experience Page</h3>
    </div>
    <div class="col-md-8">
      <div class="panel-titleo">
        {{-- <a href="{{ url('experience') }}" class="tabs {{ url('business') == request()->url() ? 'tabs-active' : '' }}">Content Page</a> --}}
        {{-- <a href="{{ url('business.edit', $about->id)}}" class="tabs {{ url('about_update') == request()->url() ? 'tabs-active' : '' }}">Update page</a> --}}
      </div>


      <div class="panel panel-default tab-top">
        <div class="panel-body">
          
          {!! Form::model($experience, ['route' => ['experiences.update', $experience], 'method' => 'patch'])!!}
              @include('experiences.form', ['model' => $experience])
          {!! Form::close() !!}

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/our_business')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection
