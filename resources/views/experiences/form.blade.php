<div class="form-group">
	{!! Form::label('client_name', 'Nama klien') !!}
	{!! Form::text('client_name', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('project_title', 'Nama proyek') !!}
	{!! Form::text('project_title', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('sector', 'Sektor') !!}
	{!! Form::text('sector', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('service', 'Service') !!}
	{!! Form::text('service', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('location', 'Lokasi proyek') !!}
	{!! Form::text('location', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('year', 'Tahun proyek') !!}
	{!! Form::text('year', null, ['class'=>'form-control']) !!}
</div>

<a href="{{ route('experiences.index') }}" class="btn btn-warning">Batal</a>
{!! Form::submit(isset($model) ? 'Perbarui' : 'Simpan', ['class'=>'btn btn-primary']) !!}
