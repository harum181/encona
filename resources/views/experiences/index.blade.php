@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Experience Page</h3>
    </div>
    <div class="col-md-8">

      <div class="panel panel-default tab-top">
        <div class="panel-body">
          {!! Form::open(['url' => 'experiences', 'method' => 'get', 'class' => 'form-inline', 'style' => 'margin-bottom: 20px;']) !!}
          <div class="form-group">
            {!! Form::text('keyword', isset($keyword) ? $keyword : null, ['class'=>'form-control', 'placeholder' => 'Cari info experience...']) !!}
          </div>
          {{ Form::button('<i class="fa fa-search"></i>', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
          <a href="{{ route('experiences.create') }}" style="float: right;" class="btn btn-primary"><i class="fa fa-plus fa-margino"></i>Tambah</a>
          {!! Form::close() !!}
          <table style="margin: 0;" class="table table-bordered table-stripped table-responsive">
            <thead>
              <tr>
                <td><strong>Client Name</strong></td>
                {{-- <td><strong>Project Title</strong></td> --}}
                <td><strong>Sector</strong></td>
                <td><strong>Service</strong></td>
                <td><strong>Location</strong></td>
                {{-- <td><strong>Year</strong></td> --}}
                <td><strong>Action</strong></td>
              </tr>
            </thead>
            <tbody>
              @forelse($experiences as $experience)
              <tr>
                <td>{{ $experience->client_name }}</td>
                {{-- <td>{{ $experience->project_title }}</td> --}}
                <td>{{ $experience->sector }}</td>
                <td>{{ $experience->service }}</td>
                <td>{{ $experience->location }}</td>
                {{-- <td>{{ $experience->year }}</td> --}}
                <td>
                  {!! Form::model($experience, ['route' => ['experiences.destroy', $experience], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                    <a href="{{ route('experiences.edit', $experience->id)}}"><i class="fa fa-pencil-square btn btn-success" style="padding: 1px 5px; display: inline-block; font-size: 10px;"></i></a>
                  {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger js-delete-confirm', 'style' => 'padding: 1px 5px; display: inline-block; font-size: 10px;']) !!}
                  {!! Form::close()!!}
                  {{-- <a href="{{ url('business/edit', $business->id)}}" style="font-size: 10px;" class="btn btn-primary">Update info</a> --}}
                </td>
              </tr>
              @empty
                <tr>
                  <td colspan="5">Data <strong>{{ $keyword }}</strong> tidak ditemukan</td>
                </tr>
              @endforelse
            </tbody>
          </table>
            
          {{ $experiences->links() }}

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('experience')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection
