@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Management Page</h3>
    </div>
    <div class="col-md-8">

      <div class="panel panel-default tab-top">
        <div class="panel-body">
          <div class="row">
            <h3 class="text-center" style="margin-bottom: 20px; margin-top: 0;">Management Encona</h3>
          </div>

          @foreach($managements as $management)
          <div class="col-md-4" style="padding-left: 0">
            <div class="panel panel-default">
              <div class="panel-heading fix-title">{{ $management->position }}</div>
              <div class="panel-body" style="padding: 20px;"><img src="{{ url('/img/'.$management->photo) }}" class="img-rounded index-img" style="width: 100%;"></div>
              <div class="panel-footer fix-title">
                <p>{{ $management->title }}</p>
                <a href="{{ route('managements.edit', $management->id)}}" style="font-size: 10px;" class="btn btn-primary">Update info</a>
              </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/about_us')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection
