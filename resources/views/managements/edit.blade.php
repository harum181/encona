{{-- <h1>jelo</h1> --}}
@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    Callout
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Update Management Page</h3>
    </div>
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-body">

        {!! Form::model($management, ['route' => ['managements.update', $management], 'method' => 'patch', 'files' => true])!!}  
          {!! Form::token(); !!}

            <div class="form-group">
              {!! Form::label('title', 'Title section one') !!}
              {!! Form::text('title', null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('position', 'Content management') !!}
              {!! Form::text('position', null, ['class'=>'form-control']) !!}
            </div>
            
            @if(isset($management) && $management->photo !== '')
              <div class="row">
                <div class="form-group">
                <div class="col-md-6">
                {!! Form::label('photo', 'Current Photo') !!}
                  <div class="thumbnail">
                    <img src="{{ url('/img/'.$management->photo) }}" class="img-rounded">
                  </div>
                </div>
                </div>
              </div>
            @endif

            <div class="form-group">
            {!! Form::label('photo', 'Display management (jpg, jpeg, png)') !!}
            {!! Form::file('photo') !!}
            </div>
            
            <a href="{{ route('managements.index') }}" class="btn btn-warning">Batal</a>
            {!! Form::submit(isset($model) ? 'Update' : 'Simpan', ['class'=>'btn btn-primary'] ) !!}

            {!! Form::close() !!}

        </div>
        </div>
      </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/about_us')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection