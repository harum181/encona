@extends('layouts.app-admin')

@section('admin')

  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Awards page</h3>
    </div>
    <div class="col-md-8">

      <div class="panel panel-default tab-top">
        <div class="panel-body">
          <a href="{{ route('safeties.create') }}" class="btn btn-primary"><i class="fa fa-plus fa-margino"></i>Tambah</a>
          <hr>
          <div class="row">
            <div class="col-md-12">
            <h4><i class="fa fa-star fa-margino"></i>HSE Award</h4>
            @foreach($hses as $hse)
            <div class="col-md-4" style="padding-left: 0">
              <div class="panel panel-default">
                {{-- <div class="panel-heading fix-title"></div> --}}
                <div class="panel-body nanoe" style="padding: 20px;">
                  <p style="text-align: center; font-size: 11px;">{{ $hse->name }}</p>
                  <img src="{{ url('/img/'.$hse->image) }}" data-toggle="modal" data-target="{{ '#'.$hse->id }}" class="img-rounded" style="width: 100%; height: 200px; object-fit: cover;">
                </div>
                <div class="panel-footer fix-title">
                  {!! Form::model($hse, ['route' => ['safeties.destroy', $hse], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                  <a href="{{ route('safeties.edit', $hse->id)}}" style="font-size: 10px;" class="btn btn-default"><i class="fa fa-upload fa-margino"></i>Update</a>
                  {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger js-delete-confirm', 'style' => 'padding: 5px 10px; display: inline-block; font-size: 10px;']) !!}
                  {!! Form::close()!!}
                </div>
              </div>
            </div>
            @endforeach  
            </div>
            <hr style="display: block;">
            <div class="col-md-12">
            <h4><i class="fa fa-star fa-margino"></i>Achievements</h4>
            @foreach($awards as $award)
            <div class="col-md-4" style="padding-left: 0">
              <div class="panel panel-default">
                <div class="panel-body" style="padding: 20px;">
                  <p style="text-align: center; font-size: 11px;">{{ $award->name }}</p>
                  <img src="{{ url('/img/'.$award->image) }}" data-toggle="modal" data-target="{{ '#'.$award->id }}" class="img-rounded" style="width: 100%; height: 200px; object-fit: cover;">
                </div>
                <div class="panel-footer fix-title">
                  {!! Form::model($award, ['route' => ['safeties.destroy', $award], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                  <a href="{{ route('safeties.edit', $award->id)}}" style="font-size: 10px;" class="btn btn-default"><i class="fa fa-upload fa-margino"></i>Update</a>
                  {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger js-delete-confirm', 'style' => 'padding: 5px 10px; display: inline-block; font-size: 10px;']) !!}
                  {!! Form::close()!!}
                </div>
              </div>
            </div>
            @endforeach  
            </div>
            <hr>
            <div class="col-md-12">
            <h4><i class="fa fa-star fa-margino"></i>Safety Award</h4>
            @foreach($safeties as $safety)
            <div class="col-md-4" style="padding-left: 0">
              <div class="panel panel-default">
                <div class="panel-body" style="padding: 20px;">
                  <p style="text-align: center; font-size: 11px;">{{ $safety->name }}</p>
                  <img src="{{ url('/img/'.$safety->image) }}" data-toggle="modal" data-target="{{ '#'.$safety->id }}" class="img-rounded index-img" style="width: 100%; height: 200px; object-fit: cover;">
                </div>
                <div class="panel-footer fix-title">
                  {!! Form::model($safety, ['route' => ['safeties.destroy', $safety], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                  <a href="{{ route('safeties.edit', $safety->id)}}" style="font-size: 10px;" class="btn btn-default"><i class="fa fa-upload fa-margino"></i>Update</a>
                  {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger js-delete-confirm', 'style' => 'padding: 5px 10px; display: inline-block; font-size: 10px;']) !!}
                  {!! Form::close()!!}
                </div>
              </div>
            </div>
            @endforeach  
            </div>
          </div>

        </div>
      </div>
    </div>
    
    @foreach($achievements as $achieve)
    <div class="modal fade out" id="{{ $achieve->id }}" role="dialog">
      <div class="modal-dialog">

        <div class="modal-content">
          <div class="modal-body">
            <img src="{{ url('/img/'.$achieve->image) }}" class="img-rounded index-img" style="width: 100%;">
            <p style="text-align: center; font-size: 11px;">{{ $achieve->name }}</p>
          </div>
        </div>
        
      </div>
    </div>
    @endforeach

    <div class="col-md-4">
      <a href="{{url('safety')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>
  </div>
@endsection
