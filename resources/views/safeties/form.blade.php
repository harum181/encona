<div class="form-group">
  {!! Form::label('type', 'Tipe Achievement') !!}
  {!! Form::select('type', array('1'=>'HSE', '2'=>'Award', '3'=>'Safety'), null, ['class'=>'form-control js-selectize']) !!}
</div>

<div class="form-group">
  {!! Form::label('name', 'Nama Achievement') !!}
  {!! Form::text('name', null, ['class'=>'form-control']) !!}
</div>

@if(isset($safety) && $safety->image !== '')
<div class="form-group">
{!! Form::label('image', 'Scan Achievement') !!}
  <div class="row">
    <div class="col-md-6">
      <div class="thumbnail">
        <img src="{{ url('/img/'.$safety->image) }}" class="img-rounded">
      </div>
    </div>
  </div>
</div>
@endif

<div class="form-group">
{!! Form::label('image', 'Display achievement (jpg, jpeg, png)') !!}
{!! Form::file('image') !!}
</div>

<a href="{{ route('safeties.index') }}" class="btn btn-warning">Batal</a>
{!! Form::submit(isset($model) ? 'Update' : 'Simpan', ['class'=>'btn btn-primary'] ) !!}