@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    {{-- Callout --}}
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Update Achievement</h3>
    </div>
    <div class="col-md-8">

      <div class="panel panel-default tab-top">
        <div class="panel-body">
          
          {!! Form::model($safety, ['route' => ['safeties.update', $safety], 'method' => 'patch', 'files' => true])!!}
              @include('safeties.form')
          {!! Form::close() !!}

        </div>
      </div>
    </div>

    {{-- {{ dd($user->name) }} --}}
    <div class="col-md-4">
      <a href="{{url('/our_business')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>

  </div>
@endsection
