@extends('layouts.app')
@section('content')

<!--====================== Our Business (Page Content) ==================-->
  <div class="container-fluid">
         <h1 align="left" style="margin-left: 70px; color:#ab0902; margin-top: 100px;">Safety</h1>
  </div><br><br>

 <!--========================== HSE Policy Content ======================-->
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-push-2">
        
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#hse">HSE Policy</a></li>
          <li><a data-toggle="tab" href="#award">Award</a></li>
          <li><a data-toggle="tab" href="#safety">Safety</a></li>
        </ul>
        <div class="tab-content">
          <div id="hse" class="tab-pane fade in active">
            <h3>HSE Award</h3>
            @foreach($hses as $hse)
            <div class="col-md-4">
              <a href="#" class="thumbnail">
                <img src="{{ url('img/'.$hse->image) }}" alt="HSE Award">
              </a>
            </div>
            @endforeach
          </div>
          <div id="award" class="tab-pane fade">
            <h3>Achievement</h3>
            @foreach($awards as $award)
            <div class="col-md-4">
              <a href="#" class="thumbnail">
                <img src="{{ url('img/'.$award->image) }}" alt="Safety">
              </a>
            </div>
            @endforeach
          </div>
          <div id="safety" class="tab-pane fade">
            <h3>Safety</h3>
            @foreach($safeties as $safety)
            <div class="col-md-4">
              <a href="#" class="thumbnail">
                <img src="{{ url('img/'.$safety->image) }}" alt="Safety">
              </a>
            </div>
            @endforeach
          </div>
        </div>

      </div>
    </div>
  </div>
        {{-- <div class="c-tab">
          <div class="c-tab__content">
            <div style="margin-top: 30px" class="row">
              <div class="col-sm-4 col-md-3">
                <a href="./img/Achievement1.png" class="thumbnail">
                  <img src="./img/Achievement1.png" alt="Safety">
                </a>
              </div>
              <div class="col-sm-4 col-md-3">
                <a href="./img/Achievement2.png" class="thumbnail">
                  <img src="./img/Achievement2.png" alt="Safety">
                </a>
              </div>
              <div class="col-sm-4 col-md-3">
                <a href="./img/Achievement3.png" class="thumbnail">
                  <img src="./img/Achievement3.png" alt="Safety">
                </a>
              </div>
              <div class="col-sm-4 col-md-3">
                <a href="./img/Achievement4.png" class="thumbnail">
                  <img src="./img/Achievement4.png" alt="Safety">
                </a>
              </div>   
            </div>
          <div class="row" style="margin-left: 170px;margin-right: 190px">
            <div class="col-sm-4 col-md-4">
                <a href="./img/Achievement6.png" class="thumbnail">
                  <img src="./img/Achievement6.png" alt="Safety">
                </a>
            </div> 
            <div class="col-sm-4 col-md-4">
              <a href="./img/Achievement5.png" class="thumbnail">
                <img src="./img/Achievement5.png" alt="Safety">
              </a>
            </div>
            <div class="col-sm-4 col-md-4">
              <a href="./img/Achievement7.png" class="thumbnail">
                <img src="./img/Achievement7.png" alt="Safety">
              </a>
            </div>    
          </div>
          </div>
        </div> --}}
        {{-- <div class="c-tab">
          <div class="c-tab__content">
            <div style="margin-left: 330px;margin-top: 20px" class="col-sm-4 col-md-4">
                <a href="./img/Poster-safety.png" class="thumbnail">
                  <img src="./img/Poster-safety.png" alt="Safety">
                </a>
            </div>
          </div>
        </div> --}}

@endsection