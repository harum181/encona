@extends('layouts.app')
@section('content')

<!--====================== Our Business (Page Content) ==================-->
  <div class="container">
  <div class="row">
  	<div class="col-md-12">
         <h1 align="left" style="margin-left: 70px; color:#ab0902; margin-top: 100px;">Our Business</h1>
  	</div>
  </div>
  </div><br><br>


 <!--=========================== Services Content =======================-->
<div class="container">
  @foreach($businesses as $business)
  <div class="row">
    <div class="col-sm-4">
      <i class="fa fa-hourglass-2 logo2 custom-logo"></i> 
    </div>
    <div class="col-sm-8">
      <h2><strong>{{ $business->title }}</strong></h2>
      <p align=justify style="font-size: 17px">{{ $business->content }}</p>
    </div>
  </div>
  @endforeach
</div>

@endsection