@extends('layouts.app')

@section('content')

<!--========================= Carousel Bootstrap =======================-->
 <header id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators (Lingkaran) -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides (Tempat Gambar) -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <div class="fill" style="background-image:url('./img/ImHomeFix1.png');"></div>
      </div>

      <div class="item">
        <div class="fill" style="background-image:url('./img/ImHomeFix2.png');"></div>
      </div>
    
      <div class="item">
        <div class="fill" style="background-image:url('./img/ImHomeFix3.png');"></div>
      </div>

      <div class="item">
        <div class="fill" style="background-image:url('./img/ImHomeFix4.png');"></div>
      </div>
      <div class="item">
        <div class="fill" style="background-image:url('./img/ImHomeFix5.png');"></div>
      </div>
    </div>

    <style type="text/css">
      /*.slido{ position: relative;top: 300px; }*/
    </style>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <i class="fa fa-chevron-left" style="position: relative; top: 30px;" aria-hidden="true"></i>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <i class="fa fa-chevron-right" style="position: relative; top: 30px;" aria-hidden="true"></i>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</header><br><br>

<!--======================== WELCOMING (Page Content) ===================-->
  <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 align="center">WELCOME to PT Encona Inti Industri Official Website !</h1>
            </div>
        </div>
  </div>


 <!--============================== SERVICES ============================-->
 <div class="container-fluid text-center bg-grey">
    <h2><strong>SERVICES</strong></h2>
    <h4>What we offer</h4>
    <br>
    <div class="row">
      <div class="col-sm-4">
        <i class="fa fa-support logo"></i>
        <h4>ENGINEERING</h4>
        <p>We provide engineers with the best quality of service</p>
      </div>
      <div class="col-sm-4">
        <i class="fa fa-dollar logo"></i>
        <h4>PROCUREMENT</h4>
        <p>We serve a procurement with a good service and quality</p>
      </div>
      <div class="col-sm-4">
        <i class="fa fa-institution logo"></i>
        <h4>CONSTRUCTION</h4>
        <p>We guarantee the results of construction</p>
      </div>
    </div><br><br><br><br>
 </div>


<!--====================== Our Customer (Page Content) ==================-->
  <div class="container-fluid parallax" >
        <h2 align="center" style="color: #e7e3e3"><strong>OUR CUSTOMER</strong></h2>
        <div class="customer">
        <img src="./img/OurCustomer 2.png" style="width: 1024px;" alt="Customer">
        </div>       
  </div>


<!--======================== Add Google Maps ===========================-->
  <div class="parallax2"><br>
  <h2 align="center"><strong>OUR LOCATION (Head Office)</strong></h2><br><br><br>
  <div id="googleMap" style="height:400px;width:100%;margin:auto;"></div>
  <script src="https://maps.googleapis.com/maps/api/js"></script>
  <script>
    var myCenter = new google.maps.LatLng(-6.282504, 106.829513);

    function initialize() {
    var mapProp = {
      center:myCenter,
      zoom:12,
      scrollwheel:false,
      draggable:false,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker = new google.maps.Marker({
      position:myCenter,
    });

    marker.setMap(map);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
  </script>
  </div>
  
  
<!--============================= CONTACT US ===============================-->
<div class="container-fluid parallax" style="min-height: 210px;color: #e7e3e3;">
  <h2 class="text-center">CONTACT US</h2><br>
  <div class="row">
    <div class="col-md-6 col-md-push-3">
    {!! Form::open(['url' => 'guest_create']) !!}
      {!! Form::token() !!}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('name', 'Nama', ['style' => 'color: white;']) !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('email', 'Email', ['style' => 'color: white;']) !!}
            {!! Form::email('email', null, ['class'=>'form-control']) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            {!! Form::label('comment', 'Comment', ['style' => 'color: white;']) !!}
            {!! Form::textarea('comment', null, ['class'=>'form-control']) !!}
          </div>
          {!! Form::submit('Simpan', ['class' => 'btn btn-default text-center']) !!}
        </div>
      </div>
    {!! Form::close() !!}
    </div>
  </div>
</div>
<a style="margin-left: 1150px" href="http://www.freepik.com">Designed by Freepik</a>

@endsection
