@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Dashboard</h3>
    </div>
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="title-top"><i class="fa fa-calendar foo"></i>Date now</div>
              <div class="date-now">{{ date('d F Y', strtotime($now)) }}</div>
              <div class="time-now">{{ date('h:m A', strtotime($now)) }}</div>
            </div>
            <div class="col-md-6">
              <div class="title-top"><i class="fa fa-cloud foo"></i>Weather</div>
              <div class="date-now"><i class="fa fa-cloud fa-margin"></i>Cloudy, 23deg</div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="panel-middle">
              <h4>Dashboard menu</h4>
              <div class="col-md-4"><a href="{{ url('guests') }}"><div class="menu"><i class="fa fa-window-maximize fa-margino"></i>Guest Book</div></a></div>
              <div class="col-md-4"><a href="{{ url('abouts') }}"><div class="menu"><i class="fa fa-calendar-o fa-margino"></i>About</div></a></div>
              <div class="col-md-4"><a href="{{ url('managements') }}"><div class="menu"><i class="fa fa-copyright fa-margino"></i>Management</div></a></div>
              <div class="col-md-4"><a href="{{ url('business') }}"><div class="menu"><i class="fa fa-clone fa-margino"></i>Our Business</div></a></div>
              <div class="col-md-4"><a href="{{ url('experiences') }}"><div class="menu"><i class="fa fa-envelope-o fa-margino"></i>Experience</div></a></div>
              <div class="col-md-4"><a href="{{ url('safeties') }}"><div class="menu"><i class="fa fa-life-ring fa-margino"></i>Safety</div></a></div>
              <div class="col-md-4"><a href=""><div class="menu"><i class="fa fa-tasks fa-margino"></i>Career</div></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <a href="{{url('/homepage')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      @include('admin.side-admin', compact('user'))
    </div>
  </div>
@endsection
