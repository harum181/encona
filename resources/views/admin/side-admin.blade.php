<div class="panel panel-default">
  <div class="panel-heading">
    <i class="fa fa-info-circle fa-margino"></i>Admin Information
  </div>
  <div class="panel-body">
    <table class="table-responsive">
      <tbody>
        <tr>
          <td class="nano"><strong>Author</strong></td>
          <td><i class="fa fa-user foo"></i>{{ $user->name }}</td>
        </tr>
        <tr>
          <td class="nano"><strong>Update info</strong></td>
          {{-- {{ dd($date_update) }} --}}
          <td><i class="fa fa-calendar foo"></i>{{ date('d F Y', strtotime($user->created_at)) }}</td>
        </tr>
        <tr>
          <td class="nano"><strong>Email address</strong></td>
          <td><i class="fa fa-envelope-o foo"></i>{{ $user->email }}</td>
        </tr>
        <tr>
          <td style="padding-top: 20px;"><a href="{{ url('home/reset_password', $user->id) }}" class="btn btn-danger btne"><i class="fa fa-lock fa-margino"></i>Update pass</a></td>
          <td style="padding-top: 20px; padding-left: 20px;"><a href="{{ url('home/edit', $user->id) }}" class="btn btn-danger btne"><i class="fa fa-user fa-margino"></i>Update info</a></td>
        </tr>
      </tbody> 
    </table>
  </div>
</div>