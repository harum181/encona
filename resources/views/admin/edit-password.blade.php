{{-- <h1>jelo</h1> --}}
@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    Callout
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Update password admin</h3>
    </div>
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-body">

        {!! Form::model($user, ['url' => ['home/reset', $user], 'method' => 'post'])!!}  
          {!! Form::token() !!}
            
            <div class="form-group">
              {!! Form::label('old_password', 'Current Password') !!}
              {!! Form::text(null, $old_password->password, ['class'=>'form-control', 'disabled']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('new_password', 'New Password') !!}
              {!! Form::text('new_password', null, ['class'=>'form-control']) !!}
            </div>
            
            <a href="{{ url('home') }}" class="btn btn-warning">Batal</a>
            {!! Form::submit(isset($model) ? 'Update' : 'Simpan', ['class'=>'btn btn-primary'] ) !!}

        {!! Form::close() !!}

        </div>
        </div>
      </div>

  </div>
@endsection