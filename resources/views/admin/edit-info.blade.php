{{-- <h1>jelo</h1> --}}
@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    Callout
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Update Info admin</h3>
    </div>
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-body">

        {!! Form::model($user, ['url' => ['home/update', $user], 'method' => 'post'])!!}  
          {!! Form::token(); !!}

            <div class="form-group">
              {!! Form::label('name', 'Admin name') !!}
              {!! Form::text('name', null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('email', 'Email admin') !!}
              {!! Form::text('email', null, ['class'=>'form-control']) !!}
            </div>
            
            <a href="{{ url('home') }}" class="btn btn-warning">Batal</a>
            {!! Form::submit(isset($model) ? 'Update' : 'Simpan', ['class'=>'btn btn-primary'] ) !!}

        {!! Form::close() !!}

        </div>
        </div>
      </div>

  </div>
@endsection