@extends('layouts.app-admin')

@section('admin')
  <div class="topi">
    Callout
  </div>
  <div class="content">
    <div class="col-md-12">
      <h3 class="title-content">Dashboard</h3>
    </div>
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="title-top"><i class="fa fa-calendar foo"></i>Date now</div>
              <div class="date-now">{{ date('d F Y', strtotime($now)) }}</div>
              <div class="time-now">{{ date('h:m A', strtotime($now)) }}</div>
            </div>
            <div class="col-md-6">
              <div class="title-top"><i class="fa fa-cloud foo"></i>Weather</div>
              <div class="date-now"><i class="fa fa-cloud fa-margin"></i>Cloudy, 23deg</div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="panel-middle">
              <h4>Dashboard menu</h4>
              <div class="col-md-4"><a href=""><div class="menu"><i class="fa fa-calendar-o fa-margino"></i>About</div></a></div>
              <div class="col-md-4"><a href=""><div class="menu"><i class="fa fa-clone fa-margino"></i>Our Business</div></a></div>
              <div class="col-md-4"><a href=""><div class="menu"><i class="fa fa-envelope-o fa-margino"></i>Experience</div></a></div>
              <div class="col-md-4"><a href=""><div class="menu"><i class="fa fa-life-ring fa-margino"></i>Safety</div></a></div>
              <div class="col-md-4"><a href=""><div class="menu"><i class="fa fa-tasks fa-margino"></i>Career</div></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <a href="{{url('/homepage')}}" class="live-preview">
        <i class="fa fa-eye fa-margino"></i>Live Preview
      </a>
      <div class="panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-info-circle fa-margino"></i>Admin Information
        </div>
        <div class="panel-body">
          <table class="table-striped">
            <tbody>
              <tr>
                <td class="nano">Author</td>
                <td><i class="fa fa-user foo"></i>{{ Auth::user()->name }}</td>
              </tr>
              <tr>
                <td class="nano">Update info</td>
                {{-- {{ dd($date_update) }} --}}
                <td><i class="fa fa-calendar foo"></i>{{ date('d F Y', strtotime($user->created_at)) }}</td>
              </tr>
              <tr>
                <td class="nano">Email address</td>
                <td><i class="fa fa-envelope-o foo"></i>{{ Auth::user()->email }}</td>
              </tr>
              <tr>
                <td colspan="2"><hr><a href="#" class="btn btn-danger btne"><i class="fa fa-user fa-margino"></i>Update info</a></td>
              </tr>
            </tbody> 
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
