@extends('layouts.app')

@section('content')

<!--======================== ABOUT US (Page Content) ====================-->
  <div class="container-fluid">
         <h1 align="left" style="margin-left: 70px; color:#ab0902; margin-top: 100px;">About Us</h1>
  </div><br><br>


 <!--============================== About Us ============================-->
 <div class="container-fluid bg-grey custom-text">
    <h3 style="color:#ab0902; text-align: center;">{{ $about->t_about }}</h3>
    <p>{{ $about->c_about }}</p>
 </div><br><br><br>


<!--=============================== Milestones ==========================-->
  <div class="container-fluid">
        <h3 align="center" style="color:#053cc8">{{ $about->t_milestone }}</h3>
        <div class="customer">
        <img src="{{ url('/img/'.$about->p_milestone) }}" style="width: 1024px;margin-top: 20px">
        </div>       
  </div><br><br><br>


<!--============================= Management ============================-->
  <div class="container-fluid custom-text">
    <div class="text-center">
      <h3 align="center" style="color:#ab0902">{{ $about->t_management }}</h3><br>
    </div>
    <div class="row">
      <div class="col-sm-4 col-md-push-4">
        <div class="panel panel-default text-center">
          <div class="panel-heading">
            <h3>{{ $chairman->position }}</h3>
          </div>
          <div class="panel-body">
            <div class="img-thumbnail">
              <img src="{{ url('/img/'.$chairman->photo) }}">
            </div>
          </div>
          <div class="panel-footer">
            <h4>{{ $chairman->title }}</h4>
          </div>
        </div> 
      </div>
    </div>
    
    <div class="row">
      @foreach($managementTops as $management)
      <div class="col-sm-4">
        <div class="panel panel-default text-center">
          <div class="panel-heading">
            <h3>{{ $management->position }}</h3>
          </div>
          <div class="panel-body">
            <div class="img-thumbnail">
              <img src="{{ url('/img/'.$management->photo) }}">
            </div>
          </div>
          <div class="panel-footer">
            <h4>{{ $management->title }}</h4>
          </div>
        </div> 
      </div>
      @endforeach 
    </div>
    <div class="row">
      @foreach($managementBottoms as $management)
      <div class="col-sm-4">
        <div class="panel panel-default text-center">
          <div class="panel-heading">
            <h3>{{ $management->position }}</h3>
          </div>
          <div class="panel-body">
            <div class="img-thumbnail">
              <img src="{{ url('/img/'.$management->photo) }}">
            </div>
          </div>
          <div class="panel-footer">
            <h4>{{ $management->title }}</h4>
          </div>
        </div> 
      </div>
      @endforeach 
    </div>


  </div>

@endsection
