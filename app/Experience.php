<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = ['client_name', 'project_title', 'sector', 'service', 'location', 'year'];
}
