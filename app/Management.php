<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Management extends Model
{
		protected $fillable = ['photo', 'title', 'position'];

    public function about()
    {
    	return $this->belongsTo('App\About');
    }
}
