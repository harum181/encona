<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use App\Http\Requests;
use App\User;
use App\About;
use App\Management;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $user = User::first();
        $about = About::first();
        $managements = Management::all();
        // dd($managements);
        
        return view('abouts.index', compact('user', 'about', 'managements'));
    }

    public function view()
    {
        
    }

    public function create()
    {
        
    }

    protected function savePhoto(UploadedFile $picture)
    {
        $fileName = str_random(10) . '.' . $picture->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
        $picture->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'img'
        . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

    public function store(Request $request)
    {
        //
    }

    public function edit($id)
    {
        $user = User::first();
        $about = About::findOrFail($id);
        $managements = Management::all();
        return view('abouts.edit', compact('about', 'user', 'managements'));
    }

    public function update(Request $request, $id)
    {
        $about = About::findOrFail($id);
        $this->validate($request, [
            't_about' => 'required',
            'c_about' => 'required',
            't_milestone' => 'required',
            'p_milestone' => 'mimes:jpeg,png,jpg|max:10240',
            't_management' => 'required'
        ]);

        $data = $request->only('t_about', 'c_about', 't_milestone', 't_management');

        // dd($data);

        if ($request->hasFile('p_milestone')) {
            $data['p_milestone'] = $this->savePhoto($request->file('p_milestone'));
            if ($about->p_milestone !== '') $this->deletePhoto($about->p_milestone);
        }

        $about->update($data);

        return redirect()->route('abouts.index');
    }

}
