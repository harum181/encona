<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Business;
use App\User;

class BusinessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$businesses = Business::all();
    	$user = User::first();

    	return view('businesses.index', compact('businesses', 'user'));
    }

    public function view()
    {
    	
    }

    public function edit($id)
    {
    	$business = Business::findOrFail($id);
    	$user = User::first();

    	return view('businesses.edit', compact('user', 'business'));
    }

    public function update(Request $request, $id)
    {
    	// dd($request);
    	$business = Business::findOrFail($id);
        // $this->validate($request, [
        //     'title' => 'required',
        //     'position' => 'required'
        // ]);
			
		$data = $request->all();
		$business->update($data);
		// dd($business);
		return redirect()->action('BusinessController@index');       
    }
}
