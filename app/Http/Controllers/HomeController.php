<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
           
    public function index()
    {
    	$user = User::first();
    	$now = Carbon::now();
    	$now->toDateTimeString();
        return view('admin.home', compact('user', 'now'));
    }

    public function editPass($id)
    {
        $user = User::findOrFail($id);
        $old_password = User::where('id', '=', $id)
                            ->select('password')
                            ->first();
        return view('admin.edit-password', compact('user', 'old_password'));
    }

    public function updatePass(Request $request, $id)
    {
        $this->validate($request, [
            // 'name' => 'max:255',
            // 'email' => 'email|max:255',
            'new_password' => 'min:6',
        ]);

        $user = User::findOrFail($id);
        $user->password = Hash::make($request->get('new_password'));
        $user->save();
        // $user->update($data);

        // dd($request);
        return redirect()->action('HomeController@index');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.edit-info', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        // $this->validate($request, [
        //     'name' => 'max:255',
        //     'email' => 'email|max:255'
        // ]);

        $data = $request->all();    
        $user->update($data);
        // $user->update($data);

        // dd($user);
        return redirect()->action('HomeController@index');
    }
}
