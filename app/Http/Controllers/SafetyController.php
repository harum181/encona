<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests;
use App\User;
use App\Safety;

class SafetyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::first();
        $hses = Safety::where('type', '=', 1)->get();
        $awards = Safety::where('type', '=', 2)->get();
        $safeties = Safety::where('type', '=', 3)->get();
        $achievements = Safety::all();
        // dd($hse);
        return view('safeties.index', compact('hses', 'awards', 'safeties', 'user', 'achievements'));
    }

    public function create()
    {
        $user = User::first();

        return view('safeties.create', compact('user', 'achievements'));
    }

    protected function savePhoto(UploadedFile $picture)
    {
        $fileName = str_random(10) . '.' . $picture->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
        $picture->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

    public function store(Request $request)
    {
        // $about = Safety::findOrFail($id);
        $this->validate($request, [
            'type' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg|max:10240',
            'name' => 'required'
        ]);

        $data = $request->only('type', 'image', 'name');
        if($request->hasFile('image')) {
            $data['image'] = $this->savePhoto($request->file('image'));
        }

        $safety = Safety::create($data);

        return redirect()->route('safeties.index');
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $user = User::first();
        $safety = Safety::findOrFail($id);
        return view('safeties.edit', compact('user', 'safety'));
    }

    public function update(Request $request, $id)
    {
        $safety = Safety::findOrFail($id);
        $this->validate($request, [
            'type' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg|max:10240',
            'name' => 'required'
        ]);

        $data = $request->only('type', 'image', 'name');

        if ($request->hasFile('image')) {
            $data['image'] = $this->savePhoto($request->file('image'));
            if ($safety->image !== '') $this->deletePhoto($safety->image);
        }

        $safety->update($data);

        return redirect()->route('safeties.index');
    }

    public function destroy($id)
    {
        $safety = Safety::find($id);
        if ($safety->image !== '') $this->deletePhoto($safety->image);
        $safety->delete();
        return redirect()->route('safeties.index');
    }
}
