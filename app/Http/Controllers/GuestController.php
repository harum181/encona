<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Guest;
use App\User;

class GuestController extends Controller
{
		public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    		$keyword = $request->get('keyword');
    		$user = User::first();
    		$guests = Guest::where('name', 'LIKE', '%'.$keyword.'%')
    										->orWhere('email', 'LIKE', '%'.$keyword.'%')
    										->orderBy('created_at')
    										->paginate(10);

    		return view('guests.index', compact('guests', 'user', 'keyword'));
    }

    public function destroy($id)
    {
	    	$guest = Guest::find($id);
        $guest->delete();
        return redirect()->action('GuestController@index');
    }
}
