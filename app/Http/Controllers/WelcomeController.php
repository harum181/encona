<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Guest;
use App\About;
use App\User;
use App\Management;
use App\Business;
use App\Experience;
use App\Safety;

class WelcomeController extends Controller
{
    public function index()
    {
    	return view('welcome');
    }

    public function aboutUs()
    {
        $user = User::first();
        $about = About::first();
        $managementTops = Management::skip(1)->take(3)->get();
        $managementBottoms = Management::skip(4)->take(3)->get();
        $chairman = Management::where('id', '=', 1)->first(['photo', 'title', 'position']);
        // dd($chairman);
        return view('about_us', compact('about', 'user', 'managementTops', 'managementBottoms', 'chairman'));
    }

    public function ourBusiness()
    {
        $businesses = Business::all();
        return view('our_business', compact('businesses'));
    }

    public function experience()
    {
        $experiences = Experience::paginate(10);

        return view('experience', compact('experiences'));
    }

    public function safety()
    {
        $hses = Safety::where('type', '=', 1)->get();
        $awards = Safety::where('type', '=', 2)->get();
        $safeties = Safety::where('type', '=', 3)->get();
        // dd($hse);
        return view('safety', compact('hses', 'awards', 'safeties'));
    }

    public function store(Request $request)
    {
    		// dd($request);
	    	$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'comment' => 'required'
        ]);
        $data = $request->only('name', 'email', 'comment');

        $guest = Guest::create($data);

        return redirect()->action('WelcomeController@index');
    }
}
