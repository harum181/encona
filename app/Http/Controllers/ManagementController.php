<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use App\Http\Requests;
use App\User;
use App\About;
use App\Management;

class ManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $user = User::first();
        $managements = Management::all();
        // dd($managements);
        
        return view('managements.index', compact('user', 'managements'));
    }

    // public function view()
    // {
    //     $user = User::first();
    //     $managements = Management::skip(1)->take(6)->get();
    //     $chairman = Management::where('id', '=', 1)->first(['photo', 'title', 'position']);
    //     // dd($chairman);
    //     return view('about_us', compact('about', 'user', 'managements', 'chairman'));
    // }

    public function create()
    {
        
    }

    protected function savePhoto(UploadedFile $picture)
    {
        $fileName = str_random(10) . '.' . $picture->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
        $picture->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'img'
        . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

    public function store(Request $request)
    {
        //
    }

    public function edit($id)
    {
        $user = User::first();
        $management = Management::findOrFail($id);
        return view('managements.edit', compact('user', 'management'));
    }

    public function update(Request $request, $id)
    {   
        // dd($request);
        $management = Management::findOrFail($id);
        $this->validate($request, [
            'photo' => 'mimes:jpeg,png,jpg|max:10240',
            'title' => 'required',
            'position' => 'required'
        ]);

        $data = $request->only('title', 'position');

        // dd($data);

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
            if ($management->photo !== '') $this->deletePhoto($management->photo);
        }

        $management->update($data);

        return redirect()->route('managements.index');
    }
}
