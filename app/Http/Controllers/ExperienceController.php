<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Experience;
use App\User;

class ExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $keyword = $request->get('keyword');
        $experiences = Experience::where('client_name','LIKE','%'.$keyword.'%')
                                    ->orWhere('project_title','LIKE','%'.$keyword.'%')
                                    ->orWhere('sector','LIKE','%'.$keyword.'%')
                                    ->orWhere('service','LIKE','%'.$keyword.'%')
                                    ->orWhere('location','LIKE','%'.$keyword.'%')
                                    ->orWhere('year','LIKE','%'.$keyword.'%')
                                    ->orderBy('client_name')
                                    ->paginate(20);
        $user = User::first();

        return view('experiences.index', compact('experiences', 'user', 'keyword'));
    }

    public function view()
    {
           
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::first();
        return view('experiences.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'client_name' => 'required',
            'project_title' => 'required',
            'sector' => 'required',
            'service' => 'required',
            'location' => 'required',
            'year' => 'required',
        ]);
        $data = $request->only('client_name', 'project_title', 'sector', 'service', 'location', 'year');

        $experience = Experience::create($data);

        return redirect()->route('experiences.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $experience = Experience::findOrFail($id);
        $user = User::first();

        return view('experiences.edit', compact('experience', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $experience = Experience::findOrFail($id);
        $this->validate($request, [
            'client_name' => 'required',
            'project_title' => 'required',
            'sector' => 'required',
            'service' => 'required',
            'location' => 'required',
            'year' => 'required',
        ]);
        $data = $request->only('client_name', 'project_title', 'sector', 'service', 'location', 'year');

        $experience->update($data);

        return redirect()->route('experiences.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $experience = Experience::find($id);
        $experience->delete();
        return redirect()->route('experiences.index');
    }
}
