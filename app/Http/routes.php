<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () { return view('welcome'); });
Route::get('homepage', 'WelcomeController@index');

Route::group(['middleware'=>'web'], function() {
	Route::auth();
	Route::get('/home', 'HomeController@index');
	Route::get('home/reset_password/{id}', 'HomeController@editPass');
	Route::post('home/reset/{id}', 'HomeController@updatePass');
	Route::get('home/edit/{id}', 'HomeController@edit');
	Route::post('home/update/{id}', 'HomeController@update');
	Route::get('career', function(){ return view('career'); });
	Route::get('about_us', 'WelcomeController@aboutUs');
	Route::resource('abouts', 'AboutController');
	Route::resource('managements', 'ManagementController');
	Route::get('our_business', 'WelcomeController@ourBusiness');
	Route::get('business', 'BusinessController@index');
	Route::get('business/edit/{id}', 'BusinessController@edit');
	Route::post('business/{id}', 'BusinessController@update');
	Route::resource('experiences', 'ExperienceController');
	Route::get('experience', 'WelcomeController@experience');
	Route::get('guests', 'GuestController@index');
	Route::post('guest_create', 'WelcomeController@store');
	Route::delete('guest_destroy/{id}', 'GuestController@destroy');
	Route::get('guest_search', 'GuestController@index');
	Route::get('safety', 'WelcomeController@safety');
	Route::resource('safeties', 'SafetyController');
});
