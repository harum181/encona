<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
		protected $fillable = ['t_about', 'c_about', 't_milestone', 'p_milestone', 't_management'];

    public function managements()
    {
    	return $this->hasMany('App\Managenment');
    }
}
